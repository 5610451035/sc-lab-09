package Compare;

public class Product implements Taxable, Comparable<Product>{
	private String productName;
	private double price;
	
	public Product (String productName,double price){
		this.productName = productName;
		this.price = price;
	}
	
	public double getTax() {
	return price*0.07;
	}
	
	public String getProName(){
		return productName;
	}
	
	@Override
	public int compareTo(Product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
	
	public String toString(){
		return "Produt:"+productName+"  Price:"+price;
	}

}
