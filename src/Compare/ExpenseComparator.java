package Compare;
import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		double exp1 = o1.getExpense();
		double exp2 = o2.getExpense();
		if (exp1 > exp2) return 1;
		if (exp1 < exp2) return -1;
		return 0;
	}

}
