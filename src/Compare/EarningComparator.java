package Compare;
import java.util.Comparator;


public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		double earn1 = o1.getIncome();
		double earn2 = o2.getIncome();
		if (earn1 > earn2) return 1;
		if (earn1 < earn2) return -1;
		return 0;
	}

}
