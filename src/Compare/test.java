package Compare;

import java.util.ArrayList;
import java.util.Collections;

public class test {
	public static void main(String[] args){
		
		System.out.println("***Test Comparable Person***");
		ArrayList<Person> personlist = new ArrayList<Person>();
		personlist.add(new Person("John",500000));
		personlist.add(new Person("Sarah",200000));
		personlist.add(new Person("Mark",502000));
		personlist.add(new Person("Alice",555000));
		System.out.println("---- Before sort");
		for (Person c : personlist) {
			System.out.println(c);
		}
		Collections.sort(personlist);
		System.out.println("\n---- After sort with Earning");
		for (Person c : personlist) {
			System.out.println(c);
		}
		System.out.println("----------------------------------------------------------------------");
		
		
		System.out.println("***Test Comparable Product***");
		ArrayList<Product> prolist = new ArrayList<Product>();
		prolist.add(new Product("T.V.",50000));
		prolist.add(new Product("Mobile",20000));
		prolist.add(new Product("Cmputer",30000));
		prolist.add(new Product("Car",2000000));
		System.out.println("---- Before sort");
		for (Product c : prolist) {
			System.out.println(c);
		}
		Collections.sort(prolist);
		System.out.println("\n---- After sort with Price");
		for (Product c : prolist) {
			System.out.println(c);
		}
		System.out.println("----------------------------------------------------------------------");
		
		
		System.out.println("***Test Comparator Company***");
		ArrayList<Company> comlist = new ArrayList<Company>();
		comlist.add(new Company("AAA",500000,200000));
		comlist.add(new Company("BBB",20000,5000));
		comlist.add(new Company("CCC",50,1));
		comlist.add(new Company("DDD",500,200));
		
		System.out.println("---- Before sort");
		for (Company c : comlist) {
			System.out.println(c);
		}
		
		Collections.sort(comlist, new EarningComparator());
		System.out.println("\n---- After sort with Earning");
		for (Company c : comlist) {
			System.out.println(c);
		}
		
		Collections.sort(comlist, new ExpenseComparator());
		System.out.println("\n---- After sort with Expense");
		for (Company c : comlist) {
			System.out.println(c);
		}
		
		Collections.sort(comlist, new ProfitComparator());
		System.out.println("\n---- After sort with Profit");
		for (Company c : comlist) {
			System.out.println(c);
		}
		System.out.println("----------------------------------------------------------------------");

		
		System.out.println("***Test Comparable Taxable***");
		ArrayList<Taxable> taxlist = new ArrayList<Taxable>();
		taxlist.add(new Person("Alan",500000));
		taxlist.add(new Person("George",20000));
		taxlist.add(new Product("Computer",35690));
		taxlist.add(new Product("Clothing",3200));
		taxlist.add(new Company("A'Cop",789000,500000));
		taxlist.add(new Company("B'cop",320000,300000));
		
		
		System.out.println("---- Before sort");
		for (Taxable c : taxlist) {
			System.out.println(c);
		}
		
		Collections.sort(taxlist, new TaxComparator());
		System.out.println("\n---- After sort with Tax");
		for (Taxable c : taxlist) {
			System.out.println(c);
		}
		
	}
	
 
}
