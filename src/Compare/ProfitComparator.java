package Compare;
import java.util.Comparator;


public class ProfitComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		double pro1 = o1.getProfit();
		double pro2 = o2.getProfit();
		if (pro1 > pro2) return 1;
		if (pro1 < pro2) return -1;
		return 0;
	}

}
