package Compare;

public class Person implements Taxable, Comparable<Person>{
	private String name;
	private double salaryYear;
	public Person(String name,double salary){
		this.name = name;
		this.salaryYear = salary;
	}
	
	public double getTax() {
		if(salaryYear <= 300000){return salaryYear*0.05;}
		else {return (300000*0.05) + ((salaryYear-300000)*0.1);}
	}
	
	public String getName(){
		return name;
	}

	@Override
	public int compareTo(Person other) {
		if (this.salaryYear < other.salaryYear ) { return -1; }
		if (this.salaryYear > other.salaryYear ) { return 1;  }
		return 0;
	}
	
	public String toString(){
		return "Name:"+name+"  Salary/year:"+salaryYear;
	}

	

	
}
