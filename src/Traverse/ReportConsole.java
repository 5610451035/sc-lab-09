package Traverse;


public class ReportConsole {
	
	public void display(Node node,Traversal travel){
		
		for (Node e : travel.traverse(node)) {
			System.out.print(e.getValue()+"\t");
		}
		System.out.println();
		
	}
}
