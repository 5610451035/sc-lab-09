package Traverse;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal{

	ArrayList<Node> postlist = new ArrayList<Node>();
	
	@Override
	public ArrayList<Node> traverse(Node node) {
		
		Node x = node;
		
		if(x == null){ return null;}
		
		traverse(x.getLeft());
		traverse(x.getRight()); 
		postlist.add(x);
		
		return postlist;
	
	} 
	

}
