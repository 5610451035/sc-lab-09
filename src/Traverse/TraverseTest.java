package Traverse;

public class TraverseTest {
	
	public static void main(String[] args){
	
		Node A = new Node("A",null,null);
		Node C = new Node("C",null,null);
		Node E = new Node("E",null,null);
		Node H = new Node("H",null,null);
		Node I = new Node("I",H,null);
		Node G = new Node("G",null,I);
		Node D = new Node("D",C,E);
		Node B = new Node("B",A,D);
		Node F = new Node("F",B,G);
		
		
		ReportConsole report = new ReportConsole();
		System.out.print("Traverse with PreOrderTraversal  :"+"\t");
		report.display(F,new PreOrderTraversal());
		System.out.print("Traverse with InOrderTraversal  :"+"\t");
		report.display(F,new InOrderTraversal());
		System.out.print("Traverse with PostOrderTraversal  :"+"\t");
		report.display(F,new PostOrderTraversal());
		
		
		
		
		
	}
	

}
