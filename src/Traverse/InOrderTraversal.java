package Traverse;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal{

	ArrayList<Node> inlist = new ArrayList<Node>();
	
	@Override
	public ArrayList<Node> traverse(Node node) {
		
		Node x = node;
		
		if(x == null){ return null;}
		  
		traverse(x.getLeft());
		inlist.add(x);
		traverse(x.getRight()); 
		
		return inlist;
	
	} 
	

}
