package Traverse;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal{

	ArrayList<Node> prelist = new ArrayList<Node>();
	
	@Override
	public ArrayList<Node> traverse(Node node) {
		
		Node x = node;
		
		if(x == null){ return null;}
		
		prelist.add(x);
		traverse(x.getLeft());
		traverse(x.getRight()); 
		
		return prelist;
	
	} 
	
	
	

}
